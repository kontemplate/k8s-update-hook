#!/bin/bash
set -ueo pipefail

# This script updates the default version of a kontemplate resource set in the
# Kubernetes configuration repository.
#
# It should be called from Gitlab CI with the following variables supplied:
#
#   GITLAB_K8S_DEPLOY_KEY: The SSH private key to use for cloning & pushing.
#   K8S_RESOURCE_SET:      The name of the resource set to update
#   NEW_VERSION:           The version to set the default to
#   SRC_JOB_ID:            The Gitlab CI job ID that triggered this deployment
#   SRC_USER_EMAIL:        The Gitlab CI user email that triggered the source job

readonly WORKSPACE="${HOME}/workspace"
readonly REPO_URL='git@gitlab.com:kontemplate/kubernetes.git'
readonly COMMIT_TEMPLATE='chore %s: Update default version to %s\n\nTriggered by job %s (user %s)'
readonly TMP_LOCATION='/tmp/updated-default.json'

# Git boilerplate setup ...
function prepare_git() {
    git config --global user.email "gitlab@kontemplate.works"
    git config --global user.name "Gitlab CI"
}

# Prepares the SSH-key supplied by Gitlab via an environment variable for use
# and updates the Gitlab host key.
function prepare_ssh() {
    mkdir -p ~/.ssh
    echo "${GITLAB_K8S_DEPLOY_KEY}" > ~/.ssh/id_rsa
    ssh-keyscan gitlab.com > ~/.ssh/known_hosts
    chmod 0400 ~/.ssh/*
}

# Checks out the current Kubernetes repository in a workspace
function prepare_workspace {
    git clone "${REPO_URL}" "${WORKSPACE}"
}

# Performs the version update and commits it
function perform_change {
    cd "${WORKSPACE}"
    local TARGET_FILE="${K8S_RESOURCE_SET}/default.json"

    echo "Updating ${TARGET_FILE} .version to ${NEW_VERSION}"

    jq --arg version "${NEW_VERSION}" '.version = $version' "${TARGET_FILE}" > "${TMP_LOCATION}"
    mv "${TMP_LOCATION}" "${TARGET_FILE}"

    echo -n "Changes made: "
    git diff "${TARGET_FILE}"

    git add "${TARGET_FILE}"
    local MSG=$(printf "${COMMIT_TEMPLATE}" \
                       "${K8S_RESOURCE_SET}" "${NEW_VERSION}" \
                       "${SRC_JOB_ID}" "${SRC_USER_EMAIL}")

    git commit --author 'Gitlab CI <gitlab@kontemplate.works>' -m "${MSG}"
}

function perform_push {
    cd "${WORKSPACE}"
    git push
}

prepare_git
prepare_ssh
prepare_workspace
perform_change
perform_push
