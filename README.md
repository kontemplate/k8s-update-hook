Update Hook
===========

This repository is part of the [kontemplate example project][]
demonstrating how to set up a CI flow for automatically deploying
Kubernetes clusters using Kontemplate.

------------------------------------------------------------

The job of the update hook is to be called at the end of every
successful CI pipeline with information about which resource set has
changed and what its new default version should be.

It clones the repository containing actual Kontemplate/Kubernetes
configuration and commits the version change into that repository.

The commit in the Kontemplate/Kubernetes repository will in turn cause
the Kubernetes CI pipeline to run.

[kontemplate example project]: https://gitlab.com/kontemplate/
