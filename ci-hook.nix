# This file builds the Docker image used for running the
# 'deploy-commit.sh' script.
#
# It's simply an image with curl & jq installed, nothing fancy.
#
# Build with:
#   nix-build ci-hook.nix
#   docker load -i result
#

{ pkgs ? import <nixpkgs> {} }:

with pkgs; dockerTools.buildImage {
  name = "deploy-hook";
  contents = [ bash curl cacert jq ];
}
